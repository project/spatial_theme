CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Spatial is a simple and spacious theme with minimalistic design topped off with
a large cover image. The theme is 100% responsive, build with bootstrap.

 * For a full description of the theme, visit the project page:
   https://www.drupal.org/project/spatial_theme

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/spatial_theme


SPATIAL THEME INSTALLATION
--------------------------

Note: As this theme is based on bootstrap you should download and enable
Drupal Bootrap project. This can be downloaded from
https://www.drupal.org/project/bootstrap.

The installation process of Spatial theme is of a straight forward
procedure. Follow steps for installation.

 - Theme file can be downloaded from the link
   https://www.drupal.org/project/spatial_theme
 - Extract the downloaded file to the themes directory.
 - Goto Admin > Appearance, find spatial theme and choose 'Install and
   set as default' option.
 - You have now enabled your theme.


CONFIGURATION
-------------

The theme sections can be customized from the theme settings in admin area.
Sections include:

 - Social Media Link.
 - Home page settings

Note that, there are 2 supporting features along with the Spatial theme
which can be found in Administer > Modules.
 - Spatial Theme Gallery
 - Spatial Theme Testimonials

The 'Spatial Theme Gallery' and 'Spatial Theme Testimonials' features should be
enabled to display Gallery and Testimonials block.

    1. Enabling Spatial Theme Gallery.

For enabling Spatial Theme Gallery goto Administrator > Modules and enable
'Spatial Theme Gallery'. On enabling 'Spatial Theme Gallery'
corresponding content type 'Gallery' will be created.

    2. Enabling Spatial Theme Testimonials.

For enabling Spatial Theme Testimonials goto Administrator > Modules and enable
'Spatial Theme Testimonials'. On enabling 'Spatial Theme Testimonials'
corresponding content type 'Testimonials' will be created.


MAINTAINERS
-----------

Current maintainers:
 * Zyxware - https://www.drupal.org/u/zyxware
